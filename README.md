# Знакомство с PyGame и git

## Домашнее задание

Зарегистрироваться на сайте GitHub (или GitLab или Bitbucket), создать там репозиторий, в котором разместить программу, рисующую флаг РФ с помощью библиотеки Pygame. В репозитории смоделировать ветвление (не менее трех веток), конфликт изменений и решение конфликта.

Например: первый коммит рисует флагшток, затем начинается ветвление: в одной ветке рисуется белый сегмент, в другой — синий. Затем эти ветки объединяются. Следом ветвление: в одной — синий сегмент, в другой — сегмент другого цвета. Затем объединение с решением конфликта.

# Contributing

Check https://www.conventionalcommits.org for commit messages convention.

Check https://www.gitflow.com for branching convention. 

# Changelog

##  (2019-10-02)

* feat: add flag drawing ([54c0406](https://gitlab.com/ShGKme/pygame-git-hw/commit/54c0406))
* feat: add flagpole drawing ([40c3b22](https://gitlab.com/ShGKme/pygame-git-hw/commit/40c3b22))
* chore: add gitignore ([32ea927](https://gitlab.com/ShGKme/pygame-git-hw/commit/32ea927))
* chore: add idea config ([a784e4b](https://gitlab.com/ShGKme/pygame-git-hw/commit/a784e4b))
* chore: init pygame ([5972ad0](https://gitlab.com/ShGKme/pygame-git-hw/commit/5972ad0))
* chore: initial commit ([225db1c](https://gitlab.com/ShGKme/pygame-git-hw/commit/225db1c))
* docs(readme): init readme ([565df09](https://gitlab.com/ShGKme/pygame-git-hw/commit/565df09))



