import pygame


class PyGameRussianFlag:
    SCREEN_SIZE = 500, 500

    def __init__(self):
        pygame.init()
        self.screen = pygame.display.set_mode(self.SCREEN_SIZE)
        self.draw()
        while pygame.event.wait().type != pygame.QUIT:
            pygame.display.flip()
        pygame.quit()

    def draw(self):
        pygame.draw.rect(self.screen, pygame.Color("cornsilk"), ((20, 20), (20, 460)))
        russian_flag_colors = (pygame.Color("#FFFFFF"), pygame.Color("#0039A6"), pygame.Color("#D52B1E"))
        width = 420
        height = width * (2 / 3)
        pos = 40, 40
        pygame.draw.rect(self.screen,
                         russian_flag_colors[0],
                         [pos, (width, height / 3)])
        pygame.draw.rect(self.screen,
                         russian_flag_colors[1],
                         [(pos[0], pos[1] + int(height / 3)), (width, int(height / 3))])
        pygame.draw.rect(self.screen,
                         russian_flag_colors[2],
                         [(pos[0], pos[1] + int(height / 3 * 2)), (width, int(height / 3))])


if __name__ == '__main__':
    russian_flag = PyGameRussianFlag()
